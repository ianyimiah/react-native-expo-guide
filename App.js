import React, { useState } from 'react';
import uuid from 'uuid/v4';
import { View, Button, StyleSheet, FlatList } from 'react-native';
import GoalItem from './components/GoalItem';
import GoalInput from './components/GoalInput';

export default function App() {
  const [courseGoals, setCourseGoals] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false);

  const addGoalHandler = (newGoal) => {
    console.log(newGoal);
    setCourseGoals(currentGoals => [
      ...currentGoals,
      { id: uuid(), value: newGoal }
    ]);
    setIsAddMode(false);
  }

  const removeGoalHandler = goalId => {
    setCourseGoals(currentGoals => currentGoals.filter(goal => goal.id !== goalId));
  }

  const cancelGoalAdditionHandler = () => {
    console.log('uio');
    setIsAddMode(false);
  }

  return (
    <View style={styles.screen}>
      <View style={styles.button}><Button title="Add New Goal" onPress={() => setIsAddMode(true)} /></View>
      <GoalInput visible={isAddMode} onAddGoal={addGoalHandler} onCancel={cancelGoalAdditionHandler} />
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={courseGoals}
        renderItem={itemData => <GoalItem
          onDelete={removeGoalHandler.bind(this, itemData.item.id)}
          title={itemData.item.value} />}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 50
  },
  button: {
    marginBottom: 12
  }
});